# typescript-beyond-the-basics
code examples for the talk: Typescript - beyond the basics

## Part 1

- type inference and literal types
- intersection and union types
- keywords: `typeof`, `keyof` and `as const`
- type narrowing
- generics
- fixing `Object.keys`

## Part 2

- quick recap on generics
- writing a typesafe omit function
    - naive approach
    - intellisense for second argument
    - better type for the return value
    - typesafe approach
- List Component with Categories
- Custom type Guards

## Resources:

- [Blog post](https://dev.to/jarvispact/typescript-beyond-the-basics-2ap0)
- [Recording of Part 1](https://confluence.netconomy.net/x/J9K0DQ)