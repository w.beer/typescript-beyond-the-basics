/* eslint-disable prettier/prettier */

// ===============================
// 01-intersection-and-union-types

{
    type Intersection = { a: string } & { b: number };
    const test1: Intersection = { a: 'a', b: 1 }; // ok
    // const test2: Intersection = { a: 'a' }; // Property 'b' is missing in type '{ a: string; }' but required in type '{ b: number; }'
}
{
    type Union = { a: string } | { a: number };
    const test1: Union = { a: 'a' }; // ok
    const test2: Union = { a: 1 }; // ok
}

// =================
// inferface vs type

// you cannot use intersection and union types with interface types,
// but you can wrap your existing interfaces into a type or use extends for intersections

{
    interface A { a: string }
    interface B extends A { b: number }
    const test1: B = { a: 'a', b: 1 }; // ok
    // const test2: B = { a: 'a' }; // Property 'b' is missing in type '{ a: string; }' but required in type 'B'
}
{
    interface A { a: string }
    interface B { a: number }
    type Union = A | B;
    const test1: Union = { a: 'a' }; // ok
    const test2: Union = { a: 1 }; // ok
}

/* eslint-enable prettier/prettier */

// ==================
// real world example

{
    type Product = {
        code: string;
        name?: string;
    };

    type Entry = {
        code: string;
        statusCode: string;
        statusDescription: string;
        quantity?: number;
        product?: Product;
    };

    type Order = {
        code: string;
        deliveryStatusCode: string;
        deliveryStatus: string;
        entry?: Entry;
        product?: Product;
    };

    const order = null as unknown as Order;
}

// actually those are 3 distinct types. Working with a type like this leads to "defensive programming" => a lot of null, undefined checks in the code

{
    type OrderBase = {
        code: string;
        deliveryStatusCode: string;
        deliveryStatus: string;
    };

    type EntryBase = {
        code: string;
        statusCode: string;
        statusDescription: string;
    };

    type OrderListItem = OrderBase & {
        kind: 'OrderListItem';
        product: {
            code: string;
        };
    };

    type OrderPreview = OrderBase & {
        kind: 'OrderPreview';
        entry: EntryBase;
    };

    type OrderDetail = OrderBase & {
        kind: 'OrderDetail';
        entry: EntryBase & {
            quantity: number;
            product: {
                code: string;
                name: string;
            };
        };
    };

    type UnknownOrder = OrderListItem | OrderPreview | OrderDetail;

    const unknownOrder = null as unknown as UnknownOrder;
    const listItemOrder = null as unknown as OrderListItem;
    const previewOrder = null as unknown as OrderPreview;
    const detailOrder = null as unknown as OrderDetail;

    const processOrder = (order: UnknownOrder) => {
        switch (order.kind) {
            case 'OrderListItem':
                order.product.code;
                return 'OrderListItem';
            case 'OrderPreview':
                order.entry.code;
                return 'OrderPreview';
            default:
                // typescript knows that the only type left is the type: OrderDetail
                order.entry.product.name;
                return 'OrderDetail';
        }
    };
}
