/* eslint-disable @typescript-eslint/no-explicit-any */
import React from 'react';

// https://stackblitz.com/edit/react-ts-3bckrk?file=index.tsx

{
    type Props = {
        categories: { key: string; label: string }[];
        items: { category: string; label: string }[];
    };

    const filterItemForCategory =
        (category: Props['categories'][number]) =>
        (item: Props['items'][number]) =>
            item.category === category.key;

    const List = ({ categories, items }: Props) => {
        return (
            <ul>
                {categories.map((category) => (
                    <section key={category.key}>
                        <h3>{category.label}</h3>
                        <ul>
                            {items
                                .filter(filterItemForCategory(category))
                                .map((item) => (
                                    <li key={item.label}>{item.label}</li>
                                ))}
                        </ul>
                    </section>
                ))}
            </ul>
        );
    };

    const Test = () => {
        return (
            <List
                categories={[
                    { key: 'key1', label: 'Category2' },
                    { key: 'key2', label: 'Category2' },
                ]}
                items={[
                    { category: 'key1', label: 'Item1' },
                    { category: 'key1', label: 'Item2' },
                    { category: 'key2', label: 'Item3' },
                    { category: 'key3', label: 'Item4' },
                ]}
            />
        );
    };
}
{
    type Props<CategoryKey extends string, ItemKey extends CategoryKey> = {
        categories: { key: CategoryKey; label: string }[];
        items: { category: ItemKey; label: string }[];
    };

    const filterItemForCategory =
        (category: Props<string, string>['categories'][number]) =>
        (item: Props<string, string>['items'][number]) =>
            item.category === category.key;

    const List = <CategoryKey extends string, ItemKey extends CategoryKey>({
        categories,
        items,
    }: Props<CategoryKey, ItemKey>) => {
        return (
            <ul>
                {categories.map((category) => (
                    <section key={category.key}>
                        <h3>{category.label}</h3>
                        <ul>
                            {items
                                .filter(filterItemForCategory(category))
                                .map((item) => (
                                    <li key={item.label}>{item.label}</li>
                                ))}
                        </ul>
                    </section>
                ))}
            </ul>
        );
    };

    const Test = () => {
        return (
            <List
                categories={[
                    { key: 'key1', label: 'Category1' },
                    { key: 'key2', label: 'Category2' },
                ]}
                items={[
                    { category: 'key1', label: 'Item1' },
                    { category: 'key1', label: 'Item2' },
                    { category: 'key2', label: 'Item3' },
                    // { category: 'key3', label: 'Item4' },
                ]}
            />
        );
    };
}
