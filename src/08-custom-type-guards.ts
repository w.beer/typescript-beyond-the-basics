{
    const isString = (arg: unknown): arg is string => typeof arg === 'string';
}
{
    const isString = (arg: string | number): arg is string =>
        typeof arg === 'string';

    const test = (arg: string | number) => {
        if (isString(arg)) {
            return arg.toLowerCase();
        } else {
            // typeof arg = number
            return `${arg}`.toLowerCase();
        }
    };
}

// ===========
// the problem

{
    type Order = {
        code: string;
        deliveryStatusCode: string;
        deliveryStatus: string;

        product?: {
            code: string;
        };

        entry?: {
            code: string;
            statusCode: string;
            statusDescription: string;

            quantity?: number;
            product?: {
                code: string;
                name: string;
            };
        };
    };

    // the types are not explicit but hidden behind some conditions without further context
    const processOrder = (order: Order) => {
        if (order.product) {
            // do something
        }
        if (order.entry) {
            // do something

            if (order.entry.product) {
                // do something more
            }
        }
    };
}

// ===========================
// solution with kind property

{
    type OrderListItem = {
        kind: 'OrderListItem';

        code: string;
        deliveryStatusCode: string;
        deliveryStatus: string;

        // unique properties
        product: {
            code: string;
        };
    };

    type OrderPreview = {
        kind: 'OrderPreview';

        code: string;
        deliveryStatusCode: string;
        deliveryStatus: string;

        // unique properties
        entry: {
            code: string;
            statusCode: string;
            statusDescription: string;
        };
    };

    type OrderDetail = {
        kind: 'OrderDetail';

        code: string;
        deliveryStatusCode: string;
        deliveryStatus: string;

        entry: {
            code: string;
            statusCode: string;
            statusDescription: string;

            // unique properties
            quantity: number;
            product: {
                code: string;
                name: string;
            };
        };
    };

    type UnknownOrder = OrderListItem | OrderPreview | OrderDetail;

    const handleOrderListItem = (order: OrderListItem) =>
        console.log(order.product);

    const handleOrderPreview = (order: OrderPreview) =>
        console.log(order.entry.code);

    const handleOrderDetail = (order: OrderDetail) =>
        console.log(order.entry.product.code);

    const processOrder = (order: UnknownOrder) => {
        switch (order.kind) {
            case 'OrderListItem':
                return handleOrderListItem(order);
            case 'OrderPreview':
                return handleOrderPreview(order);
            default:
                return handleOrderDetail(order);
        }
    };
}

// ==============================
// solution without kind property

{
    type OrderListItem = {
        code: string;
        deliveryStatusCode: string;
        deliveryStatus: string;

        // unique properties
        product: {
            code: string;
        };
    };

    type OrderPreview = {
        code: string;
        deliveryStatusCode: string;
        deliveryStatus: string;

        // unique properties
        entry: {
            code: string;
            statusCode: string;
            statusDescription: string;
        };
    };

    type OrderDetail = {
        code: string;
        deliveryStatusCode: string;
        deliveryStatus: string;

        entry: {
            code: string;
            statusCode: string;
            statusDescription: string;

            // unique properties
            quantity: number;
            product: {
                code: string;
                name: string;
            };
        };
    };

    type UnknownOrder = OrderListItem | OrderPreview | OrderDetail;

    const handleOrderListItem = (order: OrderListItem) =>
        console.log(order.product);

    const handleOrderPreview = (order: OrderPreview) =>
        console.log(order.entry.code);

    const handleOrderDetail = (order: OrderDetail) =>
        console.log(order.entry.product.code);

    const isOrderListItem = (order: UnknownOrder): order is OrderListItem =>
        'product' in order;

    const isOrderPreview = (order: UnknownOrder): order is OrderPreview =>
        'entry' in order && !('product' in order.entry);

    const isOrderDetail = (order: UnknownOrder): order is OrderDetail =>
        'entry' in order && 'product' in order.entry;

    const processOrder = (order: UnknownOrder) => {
        if (isOrderListItem(order)) {
            return handleOrderListItem(order);
        } else if (isOrderDetail(order)) {
            return handleOrderDetail(order);
        } else if (isOrderPreview(order)) {
            return handleOrderPreview(order);
        } else {
            order; // typeof order = never
            throw new Error('received invalid order');
        }
    };
}
